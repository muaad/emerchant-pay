class CreateTransactionService
  def self.call(params, merchant_id)
    transaction = AuthorizeTransaction.find_or_initialize_by(uuid: params[:uuid])
    transaction.amount = params[:amount]
    transaction.status = 'initiated'
    transaction.merchant_id = merchant_id
    transaction.customer_email = params[:customer_email]
    transaction.customer_phone = params[:customer_phone]

    unless transaction.valid?
      return { body: { error: 'Transaction could not be created. Validations failed.' },
               status: :internal_server_error }
    end

    unless transaction.new_record?
      return { body: { error: 'Transaction could not be created. UUID provided already exists.' },
               status: :internal_server_error }
    end

    begin
      transaction.save!
    rescue StandardError
      return { body: { error: 'Transaction could not be created' }, status: :internal_server_error }
    end

    { body: transaction, status: 200 }
  end
end
