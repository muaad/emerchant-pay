require 'rails_helper'

require_relative '../../../lib/uploader/user_uploader'
require_relative '../../../lib/uploader/admin_uploader'
require_relative '../../../lib/uploader/merchant_uploader'
require_relative '../../../lib/uploader/unknown_user_type_error'

describe Uploader::UserUploader do
  context 'uploaders' do
    it 'returns the correct uploader object' do
      uploader = described_class.new(type: 'admin', file: 'admins.csv').uploader
      expect(uploader).to be_a(Uploader::AdminUploader)

      uploader = described_class.new(type: 'merchant', file: 'merchants.csv').uploader
      expect(uploader).to be_a(Uploader::MerchantUploader)
    end

    it 'uploads file and creates records for admins' do
      admin_file = File.expand_path('../data/admins.csv', __dir__)
      uploader = described_class.new(type: 'admin', file: admin_file)

      expect { uploader.upload }.to change { Admin.count }.by(1)
    end

    it 'uploads file and creates records for merchants' do
      merchant_file = File.expand_path('../data/merchants.csv', __dir__)
      uploader = described_class.new(type: 'merchant', file: merchant_file)

      expect { uploader.upload }.to change { Merchant.count }.by(1)
    end
  end
end
