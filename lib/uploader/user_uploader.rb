require_relative 'admin_uploader'
require_relative 'merchant_uploader'
require_relative 'unknown_user_type_error'
require_relative '../parsers/csv_parser'

module Uploader
  class UserUploader
    def initialize(type:, file:, parser: Parsers::CsvParser)
      @type = type
      @file = file
      @parser = parser
    end

    def uploader
      uploaders = {
        'admin' => AdminUploader,
        'merchant' => MerchantUploader
      }

      uploader_class = uploaders[@type]
      if uploader_class.nil?
        raise UnknownUserTypeError, "#{@type} is not a known user type. Try one of admin and merchant."
      end

      uploader_class.new
    end

    def upload
      records = @parser.parse_file(file: @file, model: @type.capitalize.constantize)
      uploader.upload(records: records)
    end
  end
end
