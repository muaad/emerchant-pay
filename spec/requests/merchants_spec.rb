require 'rails_helper'

RSpec.describe 'Merchants', type: :request do
  include_context 'users'

  describe 'GET /show' do
    it 'returns http success' do
      get '/merchants', params: { id: saved_merchant.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /index' do
    it 'returns http success' do
      get '/merchants'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /edit' do
    it 'returns http success' do
      get "/merchants/#{saved_merchant.id}/edit"
      expect(response).to have_http_status(:success)
    end
  end
end
