module Uploader
  class AdminUploader
    def upload(records:)
      Admin.import(records, validate: true)
    end
  end
end
