class MerchantsService
  def self.call(params)
    return Merchant.search_for(params[:query]) if params[:query].present?

    Merchant.all
  end
end
