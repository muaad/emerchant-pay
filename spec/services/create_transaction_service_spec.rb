require 'rails_helper'

describe CreateTransactionService do
  include_context 'users'

  context 'Create Transaction' do
    let(:valid_params) do
      {
        uuid: SecureRandom.uuid,
        amount: 200,
        customer_email: 'valmail@example.com',
        customer_phone: '342342'
      }
    end

    let(:invalid_params) do
      {
        uuid: SecureRandom.uuid,
        customer_email: 'valmail@example.com',
        customer_phone: '342342'
      }
    end

    let(:duplicate_params) do
      {
        uuid: create(:authorize_transaction).uuid,
        amount: 200,
        customer_email: 'valmail@example.com',
        customer_phone: '342342'
      }
    end

    it 'creates transaction' do
      expect(described_class.call(valid_params, saved_merchant.id)[:body]).to eq(AuthorizeTransaction.last)
    end

    it 'returns invalid response' do
      invalid_response = { body: { error: 'Transaction could not be created. Validations failed.' },
                           status: :internal_server_error }
      expect(described_class.call(invalid_params, saved_merchant.id)).to eq(invalid_response)
    end

    it 'returns duplicate response' do
      duplicate_response = { body: { error: 'Transaction could not be created. UUID provided already exists.' },
                             status: :internal_server_error }
      expect(described_class.call(duplicate_params, saved_merchant.id)).to eq(duplicate_response)
    end
  end
end
