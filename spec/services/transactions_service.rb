require 'rails_helper'

describe TransactionsService do
  context 'Transactions' do
    before(:each) do
      create(:refunded_transaction)
      create(:reversal_transaction)
    end

    it 'returns all transactions' do
      expect(described_class.call({})).to match_array([refunded_transaction, reversal_transaction])
    end

    it 'returns all transactions of a given type' do
      expect(described_class.call({ type: 'Refunded' })).to match_array([refunded_transaction])
    end
  end
end
