module Parsers
  module XlsxParser
    def self.parse_file(file:, model:)
      doc = SimpleXlsxReader.open(file)
      rows = doc.sheets.first.rows[1..]

      rows.map do |row|
        model.new({
                    name: row[0].strip,
                    description: row[1].strip,
                    email: row[2].strip,
                    password: '1234567'
                  })
      end
    end
  end
end
