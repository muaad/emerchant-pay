require 'rails_helper'

RSpec.describe 'PaymentTransactions', type: :request do
  describe 'GET /index' do
    it 'returns http success' do
      get '/payment_transactions'
      expect(response).to have_http_status(:success)
    end
  end
end
