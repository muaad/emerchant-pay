class CreateReversalTransactionService
  def self.call(id)
    transaction = AuthorizeTransaction.find_by(id: id)

    if transaction.nil?
      return { body: { error: 'Reversal Transaction could not be created. ID provided does not exist.' },
               status: :internal_server_error }
    end

    reversal_transaction = ReversalTransaction.find_or_initialize_by(payment_transaction_id: id,
                                                                     merchant_id: transaction.merchant_id)

    unless reversal_transaction.new_record?
      return { body: { error: 'Reversal Transaction already exists' },
               status: :internal_server_error }
    end

    reversal_transaction.uuid = SecureRandom.uuid
    reversal_transaction.status = 'reversed'
    reversal_transaction.customer_email = transaction.customer_email
    reversal_transaction.customer_phone = transaction.customer_phone

    begin
      reversal_transaction.save!

      transaction.update(status: 'reversed')
    rescue StandardError
      return { body: { error: 'Reversal Transaction could not be created' },
               status: :internal_server_error }
    end

    { body: reversal_transaction, status: 200 }
  end
end
