# == Schema Information
#
# Table name: payment_transactions
#
#  id                     :integer          not null, primary key
#  amount                 :float
#  customer_email         :string
#  customer_phone         :string
#  status                 :string
#  type                   :string
#  uuid                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  merchant_id            :integer
#  payment_transaction_id :integer
#
# Indexes
#
#  index_payment_transactions_on_merchant_id             (merchant_id)
#  index_payment_transactions_on_payment_transaction_id  (payment_transaction_id)
#  index_payment_transactions_on_status                  (status)
#  index_payment_transactions_on_type                    (type)
#  index_payment_transactions_on_uuid                    (uuid) UNIQUE
#
require 'rails_helper'

RSpec.describe PaymentTransaction, type: :model do
  context 'validations' do
    let(:transaction) { build(:payment_transaction) }
    let(:transaction_with_blank_uuid) { build(:payment_transaction, uuid: '') }
    let(:transaction_with_non_unique_uuid) { build(:payment_transaction, uuid: transaction.uuid) }
    let(:transaction_with_bad_email) do
      build(:payment_transaction, uuid: SecureRandom.uuid, customer_email: 'bad.email')
    end
    let(:transaction_with_bad_status) { build(:payment_transaction, status: 'bad') }

    it 'validates presence of uuid' do
      expect(transaction).to be_valid
      expect(transaction_with_blank_uuid).not_to be_valid
    end

    it 'validates uniqueness of uuid' do
      transaction.save
      expect(transaction_with_non_unique_uuid).not_to be_valid
    end

    it 'validates email format' do
      expect(transaction_with_bad_email).not_to be_valid
    end

    it 'validates status is one of initiated, approved, reversed, refunded and error' do
      expect(transaction_with_bad_status).not_to be_valid
    end

    context 'validates amount' do
      let(:authorize_transaction) { build(:authorize_transaction, amount: nil) }
      let(:charge_transaction) { build(:charge_transaction) }
      let(:refunded_transaction) { build(:refunded_transaction, amount: 0) }
      let(:reversal_transaction) { build(:reversal_transaction) }

      it 'validates presence of amount' do
        expect(authorize_transaction).not_to be_valid
        expect(charge_transaction).to be_valid
      end

      it 'validates amount should be greater than zero' do
        expect(refunded_transaction).not_to be_valid
        expect(charge_transaction).to be_valid
      end

      it 'validates amount should be nil for reversed transactions' do
        expect(reversal_transaction).to be_valid
        reversal_transaction.amount = 200
        expect(reversal_transaction).not_to be_valid
      end
    end
  end

  context 'callbacks' do
    let(:charge_transaction) { build(:charge_transaction) }
    let(:refunded_transaction) { build(:refunded_transaction) }
    let(:reversal_transaction) { build(:reversal_transaction) }

    it 'increments merchants total transactions value when a charge transaction is created' do
      expect(charge_transaction.merchant.total_transaction_sum).to eq(nil)

      charge_transaction.save
      expect(charge_transaction.merchant.reload.total_transaction_sum).to eq(charge_transaction.amount)
    end

    it 'decrements merchants total transactions value when a refund transaction is created' do
      total = refunded_transaction.merchant.total_transaction_sum.to_f
      expect(total).to eq(0)

      refunded_transaction.save
      expect(refunded_transaction.merchant.reload.total_transaction_sum).to eq(total - refunded_transaction.amount)
    end
  end
end
