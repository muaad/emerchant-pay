RSpec.shared_context 'users' do
  let(:user) { build(:user, password: '1234567') }

  let(:admin) { build(:admin, password: '1234567') }
  let(:merchant) { build(:merchant, password: '1234567') }

  let(:saved_merchant) { build(:merchant, password: '1234567', status: 'active') }
  let(:another_saved_merchant) { build(:merchant, name: 'Walmart', password: '1234567') }
  let(:saved_admin) { build(:admin, password: '1234567') }
  before(:each) do
    another_saved_merchant.save
    saved_merchant.save
    saved_admin.save
  end
end
