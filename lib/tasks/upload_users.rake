require_relative '../uploader/user_uploader'

namespace :users do
  desc 'Upload admins and merchants from csv'
  task :upload, %i(file_name type) => [:environment] do |_task, args|
    file_name = args[:file_name]
    type = args[:type]
    source_file = Rails.root.join('lib', 'data', file_name)

    unless File.exist?(source_file)
      raise "ERROR! Source file #{file_name} not found. Ensure it exists in `lib/data`"
    end

    # A parser could be passed here if we want to support more file types. See lib/parsers

    uploader = Uploader::UserUploader.new(file: source_file, type: type)
    uploader.upload
  end
end
