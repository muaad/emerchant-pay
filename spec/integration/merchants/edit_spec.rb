require 'rails_helper'

require 'support/shared_contexts/users'

describe 'merchants#edit', type: :system do
  include_context 'users'

  it 'shows the Merchants text and contains name of merchant to be updated' do
    visit edit_merchant_path(saved_merchant)

    expect(page).to have_content('Edit Merchant')
    expect(page).to have_field('name', with: 'Amazon')
  end

  it 'updates merchant' do
    visit edit_merchant_path(saved_merchant)

    fill_in 'name', with: 'Amazon.com'
    click_button 'Update'

    expect(page).to have_content('Merchant updated')
    expect(saved_merchant.reload.name).to eq('Amazon.com')
  end
end
