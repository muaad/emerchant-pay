Rails.application.routes.draw do
  resources :payment_transactions, only: %w(index)
  resources :merchants
  devise_for :users

  namespace :api do
    namespace :v1 do
      resources :payment_transactions, only: %w(index create)
    end
  end
end
