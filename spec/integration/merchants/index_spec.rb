require 'rails_helper'

describe '/merchants', type: :system do
  include_context 'users'

  it 'shows the Merchants text' do
    visit merchants_path
    expect(page).to have_content('Merchants')
  end

  it 'shows saved merchant names' do
    visit merchants_path
    expect(page).to have_content('Amazon')
    expect(page).to have_content('Walmart')
  end

  it 'shows merchants that match the search query' do
    visit merchants_path

    fill_in 'query', with: 'Walmart'
    click_button 'Search'
    expect(page).not_to have_content('Amazon')
    expect(page).to have_content('Walmart')
  end
end
