require 'rails_helper'

describe CreateRefundedTransactionService do
  context 'Create Refunded Transaction' do
    let(:charge_transaction) { create(:charge_transaction) }

    it 'creates refunded transaction' do
      expect(described_class.call(charge_transaction.id)[:body]).to eq(RefundedTransaction.last)
      expect(charge_transaction.reload.status).to eq('refunded')

      expect(charge_transaction.payment_transaction.reload.status).to eq('refunded')

      # Check if the same ID is provided again. Should return :duplicate.

      duplicate_response = { body: { error: 'Refund Transaction already exists' },
                             status: :internal_server_error }
      expect(described_class.call(charge_transaction.id)).to eq(duplicate_response)
    end

    it 'returns missing response' do
      missing_response = { body: { error: 'Refund Transaction could not be created. ID provided does not exist.' },
                           status: :internal_server_error }
      expect(described_class.call(32_432)).to eq(missing_response)
    end
  end
end
