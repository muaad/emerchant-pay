# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  api_key                :string
#  description            :text
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  status                 :string           default("inactive")
#  total_transaction_sum  :float
#  type                   :string           default("Merchant")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_api_key               (api_key) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_status                (status)
#  index_users_on_type                  (type)
#
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, presence: true

  scoped_search on: %i(name email description)

  # These scopes are not exactly needed here as we could just do the following thanks to STI:
  #
  #   Admin.all
  #   Merchant.all
  #
  # I just put them here to demonstrate their possible use case

  scope :admins, -> { where(type: 'Admin') }
  scope :merchants, -> { where(type: 'Merchant') }

  %w(Admin Merchant).each do |user_type|
    define_method("#{user_type.downcase}?") do
      type == user_type
    end
  end
end
