class CreateChargeTransactionService
  def self.call(id)
    transaction = AuthorizeTransaction.find_by(id: id)

    if transaction.nil?
      return { body: { error: 'Charge Transaction could not be created. ID provided does not exist.' },
               status: :internal_server_error }
    end

    charge_transaction = ChargeTransaction.find_or_initialize_by(payment_transaction_id: id,
                                                                 merchant_id: transaction.merchant_id)

    unless charge_transaction.new_record?
      return { body: { error: 'Charge Transaction already exists' },
               status: :internal_server_error }
    end

    charge_transaction.uuid = SecureRandom.uuid
    charge_transaction.amount = transaction.amount
    charge_transaction.status = 'approved'
    charge_transaction.customer_email = transaction.customer_email
    charge_transaction.customer_phone = transaction.customer_phone

    begin
      charge_transaction.save!

      transaction.update(status: 'approved')
    rescue StandardError
      return { body: { error: 'Charge Transaction could not be created' },
               status: :internal_server_error }
    end

    { body: charge_transaction, status: 200 }
  end
end
