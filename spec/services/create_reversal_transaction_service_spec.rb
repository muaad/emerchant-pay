require 'rails_helper'

describe CreateReversalTransactionService do
  context 'Create Reversal Transaction' do
    let(:authorize_transaction) { create(:authorize_transaction) }

    it 'creates reversal transaction' do
      expect(described_class.call(authorize_transaction.id)[:body]).to eq(ReversalTransaction.last)
      expect(authorize_transaction.reload.status).to eq('reversed')

      # Check if the same ID is provided again. Should return :duplicate.
      duplicate_response = { body: { error: 'Reversal Transaction already exists' },
                             status: :internal_server_error }
      expect(described_class.call(authorize_transaction.id)).to eq(duplicate_response)
    end

    it 'returns missing response' do
      missing_response = { body: { error: 'Reversal Transaction could not be created. ID provided does not exist.' },
                           status: :internal_server_error }
      expect(described_class.call(32_432)).to eq(missing_response)
    end
  end
end
