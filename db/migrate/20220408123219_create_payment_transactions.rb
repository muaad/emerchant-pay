class CreatePaymentTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :payment_transactions do |t|
      t.string :uuid
      t.float :amount
      t.string :status
      t.string :customer_email
      t.string :customer_phone
      t.integer :merchant_id, foreign_key: true

      t.timestamps
    end
    add_index :payment_transactions, :uuid, unique: true
    add_index :payment_transactions, :status
    add_index :payment_transactions, :merchant_id
  end
end
