require 'rails_helper'

describe HandleTransactionsService do
  include_context 'users'

  context 'HandleTransactions' do
    let(:authorize_transaction) { create(:authorize_transaction) }
    let(:charge_transaction) { create(:charge_transaction) }

    let(:params) do
    end

    it 'calls CreateTransactionService if uuid is in the params' do
      params = {
        uuid: SecureRandom.uuid,
        amount: 200,
        customer_email: 'valmail@example.com',
        customer_phone: '342342'
      }

      expect(CreateTransactionService).to receive(:call).with(params, saved_merchant.id)

      described_class.call(params, saved_merchant.id)
    end

    it 'calls CreateChargeTransactionService if payment_transaction_id is in the params and type is Charge' do
      params = {
        payment_transaction_id: authorize_transaction.id,
        type: 'Charge'
      }

      expect(CreateChargeTransactionService).to receive(:call).with(params[:payment_transaction_id])

      described_class.call(params, saved_merchant.id)
    end

    it 'calls CreateRefundedTransactionService if payment_transaction_id is in the params and type is Refund' do
      params = {
        payment_transaction_id: charge_transaction.id,
        type: 'Refund'
      }

      expect(CreateRefundedTransactionService).to receive(:call).with(params[:payment_transaction_id])

      described_class.call(params, saved_merchant.id)
    end

    it 'calls CreateReversalTransactionService if payment_transaction_id is in the params and type is Reversal' do
      params = {
        payment_transaction_id: charge_transaction.id,
        type: 'Reversal'
      }

      expect(CreateReversalTransactionService).to receive(:call).with(params[:payment_transaction_id])

      described_class.call(params, saved_merchant.id)
    end

    it 'returns Unknown transaction type if type is not passed in or is unrecognized' do
      params = {
        payment_transaction_id: charge_transaction.id,
        type: 'unknown'
      }

      unknow_type_response = { body: { error: 'Unknown transaction type' },
                               status: :internal_server_error }
      expect(described_class.call(params, saved_merchant.id)).to eq(unknow_type_response)
    end

    it 'returns missing uuid or payment_transaction_id if neither is provided' do
      params = {
        amount: 500
      }

      missing_uuid_response = { body: { error: 'Missing payment_transaction_id or a uuid' },
                                status: :internal_server_error }
      expect(described_class.call(params, saved_merchant.id)).to eq(missing_uuid_response)
    end
  end
end
