class AmountValidator < ActiveModel::Validator
  def validate(record)
    record.errors.add(:amount, 'cannot be blank') if record.amount.blank?
    record.errors.add(:amount, 'should be greater than zero') if record.amount && record.amount <= 0
  end
end
