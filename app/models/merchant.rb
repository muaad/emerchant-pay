# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  api_key                :string
#  description            :text
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  status                 :string           default("inactive")
#  total_transaction_sum  :float
#  type                   :string           default("Merchant")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_api_key               (api_key) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_status                (status)
#  index_users_on_type                  (type)
#
class Merchant < User
  before_save :set_api_key

  has_many :payment_transactions, dependent: :destroy

  %w(active inactive).each do |merchant_status|
    define_method("is_#{merchant_status.downcase}?") do
      status == merchant_status
    end
  end

  private

  def set_api_key
    self.api_key = SecureRandom.hex if api_key.blank?
  end
end
