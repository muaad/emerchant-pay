require 'rails_helper'

describe CreateChargeTransactionService do
  context 'Create Charge Transaction' do
    let(:authorize_transaction) { create(:authorize_transaction) }

    it 'creates charge transaction' do
      expect(described_class.call(authorize_transaction.id)[:body]).to eq(ChargeTransaction.last)

      # Check if the same ID is provided again. Should return :duplicate.

      duplicate_response = { body: { error: 'Charge Transaction already exists' },
                             status: :internal_server_error }
      expect(described_class.call(authorize_transaction.id)).to eq(duplicate_response)
    end

    it 'returns missing response' do
      missing_response = { body: { error: 'Charge Transaction could not be created. ID provided does not exist.' },
                           status: :internal_server_error }
      expect(described_class.call(32_432)).to eq(missing_response)
    end
  end
end
