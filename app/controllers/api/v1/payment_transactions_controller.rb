module Api
  module V1
    class PaymentTransactionsController < ApiController
      protect_from_forgery with: :null_session, only: [:create]

      def index
        render json: @merchant.payment_transactions
      end

      def create
        response = HandleTransactionsService.call(params, @merchant.id)

        render json: response[:body], status: response[:status]
      end
    end
  end
end
