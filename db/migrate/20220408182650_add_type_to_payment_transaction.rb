class AddTypeToPaymentTransaction < ActiveRecord::Migration[7.0]
  def change
    add_column :payment_transactions, :type, :string
    add_index :payment_transactions, :type
  end
end
