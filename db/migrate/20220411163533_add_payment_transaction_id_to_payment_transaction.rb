class AddPaymentTransactionIdToPaymentTransaction < ActiveRecord::Migration[7.0]
  def change
    add_column :payment_transactions, :payment_transaction_id, :integer, foreign_key: true
    add_index :payment_transactions, :payment_transaction_id
  end
end
