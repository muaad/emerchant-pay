require 'rails_helper'

describe MerchantsService do
  include_context 'users'

  context 'Merchants' do
    it 'returns all merchants' do
      expect(described_class.call({})).to match_array([saved_merchant, another_saved_merchant])
    end

    it 'returns all merchants whose name contains search term' do
      expect(described_class.call({ query: 'Amazon' })).to match_array([saved_merchant])
    end
  end
end
