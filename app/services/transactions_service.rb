class TransactionsService
  def self.call(params)
    return PaymentTransaction.where('type like ?', "%#{params[:type]}%") if params[:type].present?

    return PaymentTransaction.search_for(params[:query]) if params[:query].present?

    PaymentTransaction.all
  end
end
