class MerchantsController < ApplicationController
  before_action :set_merchant, only: %w(show edit update destroy)

  def show; end

  def index
    @merchants = MerchantsService.call(params)
  end

  def edit
    @form = MerchantForm.new(@merchant)
  end

  def update
    @form = MerchantForm.new(@merchant, merchant_form_params)
    if @form.update
      redirect_to merchants_path, notice: 'Merchant updated'
    else
      flash[:alert] = @merchant.errors.full_messages[0]
      redirect_to(edit_merchant_path(@merchant))
    end
  end

  def destroy
    alert_msg = 'Merchant cannot be deleted because they have transactions'

    if @merchant.payment_transactions.blank?
      @merchant.destroy
      alert_msg = 'Merchant was successfully deleted'
    end

    respond_to do |format|
      format.html { redirect_to merchants_url, alert: alert_msg }
      format.json { head :no_content }
    end
  end

  private

  def set_merchant
    @merchant = Merchant.find(params[:id])
  end

  def merchant_form_params
    params.require(:merchant_form).permit(:name, :email, :description)
  end
end
