require 'rails_helper'

describe 'payment_transactions', type: :system do
  context 'index' do
    before(:each) do
      create(:refunded_transaction)
      create(:reversal_transaction)
    end

    it 'shows the Transactions text' do
      visit payment_transactions_path
      expect(page).to have_content('Transactions')

      expect(page).to have_content('2222222')
    end

    it 'shows transactions that match the selected type' do
      visit "#{payment_transactions_path}?type=Refunded"

      expect(page).not_to have_content('4444444')
      expect(page).to have_content('2222222')
    end
  end
end
