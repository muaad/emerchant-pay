require 'rails_helper'

describe '/api/v1/payment_transactions' do
  include_context 'users'

  before(:each) do
    create(:refunded_transaction)
    create(:reversal_transaction)
  end

  context 'payment_transactions API' do
    let(:correct_api_key) { saved_merchant.api_key }
    let(:incorrect_api_key) { '23243' }
    let(:authorize_transaction) { create(:authorize_transaction) }
    let(:charge_transaction) { create(:charge_transaction) }

    context 'inactive merchant' do
      it 'returns 401 if merchant is inactive' do
        saved_merchant.update(status: 'inactive')
        get "/api/v1/payment_transactions?api_key=#{correct_api_key}"

        expect(response).to have_http_status(401)
        error_msg = 'You need to be activated before you can access the API'
        expect(JSON.parse(response.body)['error']).to eq(error_msg)
      end
    end

    context 'index' do
      it 'returns 401 if incorrect api key is sent' do
        get "/api/v1/payment_transactions?api_key=#{incorrect_api_key}"

        expect(response).to have_http_status(401)
      end

      it 'returns a list of transactions' do
        get "/api/v1/payment_transactions?api_key=#{correct_api_key}"

        expect(response).to have_http_status(200)
        expect(response.body).to eq(saved_merchant.payment_transactions.to_json)
      end
    end

    context 'create' do
      let(:valid_params) do
        {
          uuid: SecureRandom.uuid,
          amount: 200,
          customer_email: 'valmail@example.com',
          customer_phone: '342342',
          api_key: correct_api_key
        }
      end

      let(:invalid_params) do
        {
          uuid: SecureRandom.uuid,
          customer_email: 'valmail@example.com',
          customer_phone: '342342',
          api_key: correct_api_key
        }
      end

      let(:duplicate_params) do
        {
          uuid: authorize_transaction.uuid,
          amount: 200,
          customer_email: 'valmail@example.com',
          customer_phone: '342342',
          api_key: correct_api_key
        }
      end

      it 'returns 401 if incorrect api key is sent' do
        post '/api/v1/payment_transactions', params: { api_key: incorrect_api_key }

        expect(response).to have_http_status(401)
      end

      it 'creates a new transaction and returns it' do
        post '/api/v1/payment_transactions', params: valid_params

        expect(response).to have_http_status(200)
        expect(response.body).to eq(AuthorizeTransaction.last.to_json)
      end

      it 'returns duplication error if uuid provided already exists' do
        post '/api/v1/payment_transactions', params: duplicate_params

        expect(response).to have_http_status(500)
        error_msg = 'Transaction could not be created. UUID provided already exists.'
        expect(JSON.parse(response.body)['error']).to eq(error_msg)
      end

      it 'returns validation error if validation errors are raised' do
        post '/api/v1/payment_transactions', params: invalid_params

        expect(response).to have_http_status(500)
        expect(JSON.parse(response.body)['error']).to eq('Transaction could not be created. Validations failed.')
      end
    end

    context 'create charge transactions' do
      let(:params) do
        {
          api_key: correct_api_key,
          payment_transaction_id: authorize_transaction.id,
          type: 'Charge'
        }
      end

      it 'returns 401 if incorrect api key is sent' do
        post '/api/v1/payment_transactions', params: { api_key: incorrect_api_key }

        expect(response).to have_http_status(401)
      end

      it 'creates a new charge transaction and returns it' do
        expect(authorize_transaction.merchant.total_transaction_sum).to eq(nil)

        post '/api/v1/payment_transactions', params: params

        charge_transaction = ChargeTransaction.last

        expect(response).to have_http_status(200)
        expect(response.body).to eq(charge_transaction.to_json)
        expect(charge_transaction.payment_transaction).to eq(authorize_transaction)
        expect(charge_transaction.status).to eq('approved')
        expect(authorize_transaction.reload.status).to eq('approved')
        expect(authorize_transaction.merchant.reload.total_transaction_sum).to eq(charge_transaction.amount)

        # Test for trying to charge the same transaction

        post '/api/v1/payment_transactions', params: params

        expect(response).to have_http_status(500)
        expect(JSON.parse(response.body)['error']).to eq('Charge Transaction already exists')
      end

      it 'returns not found error if ID does not exist' do
        params[:payment_transaction_id] = 2322
        post '/api/v1/payment_transactions', params: params

        expect(response).to have_http_status(500)
        error_msg = 'Charge Transaction could not be created. ID provided does not exist.'
        expect(JSON.parse(response.body)['error']).to eq(error_msg)
      end
    end

    context 'create refund transactions' do
      let(:params) do
        {
          api_key: correct_api_key,
          payment_transaction_id: charge_transaction.id,
          type: 'Refund'
        }
      end

      it 'returns 401 if incorrect api key is sent' do
        post '/api/v1/payment_transactions', params: { api_key: incorrect_api_key }

        expect(response).to have_http_status(401)
      end

      it 'creates a new refund transaction and returns it' do
        post '/api/v1/payment_transactions', params: params

        total = charge_transaction.merchant.total_transaction_sum.to_f

        expect(total).to eq(150)

        refund_transaction = RefundedTransaction.last

        expect(response).to have_http_status(200)
        expect(response.body).to eq(refund_transaction.to_json)
        expect(refund_transaction.payment_transaction).to eq(charge_transaction)
        expect(refund_transaction.status).to eq('refunded')
        expect(charge_transaction.reload.status).to eq('refunded')
        expect(charge_transaction.merchant.reload.total_transaction_sum).to eq(total - refund_transaction.amount)

        # Test for trying to refund the same transaction

        params[:payment_transaction_id] = charge_transaction.id
        post '/api/v1/payment_transactions', params: params

        expect(response).to have_http_status(500)
        expect(JSON.parse(response.body)['error']).to eq('Refund Transaction already exists')
      end

      it 'returns not found error if ID does not exist' do
        params[:payment_transaction_id] = 2322
        post '/api/v1/payment_transactions', params: params

        expect(response).to have_http_status(500)
        error_msg = 'Refund Transaction could not be created. ID provided does not exist.'
        expect(JSON.parse(response.body)['error']).to eq(error_msg)
      end
    end

    context 'create reversal transactions' do
      let(:params) do
        {
          api_key: correct_api_key,
          payment_transaction_id: authorize_transaction.id,
          type: 'Reversal'
        }
      end

      it 'returns 401 if incorrect api key is sent' do
        post '/api/v1/payment_transactions', params: { api_key: incorrect_api_key }

        expect(response).to have_http_status(401)
      end

      it 'creates a new reversal transaction and returns it' do
        post '/api/v1/payment_transactions', params: params

        reversal_transaction = ReversalTransaction.last

        expect(response).to have_http_status(200)
        expect(response.body).to eq(reversal_transaction.to_json)
        expect(reversal_transaction.payment_transaction).to eq(authorize_transaction)
        expect(reversal_transaction.status).to eq('reversed')
        expect(authorize_transaction.reload.status).to eq('reversed')

        # Test for trying to reverse the same transaction

        params[:payment_transaction_id] = authorize_transaction.id
        post '/api/v1/payment_transactions', params: params

        expect(response).to have_http_status(500)
        expect(JSON.parse(response.body)['error']).to eq('Reversal Transaction already exists')
      end

      it 'returns not found error if ID does not exist' do
        params[:payment_transaction_id] = 2322
        post '/api/v1/payment_transactions', params: params

        expect(response).to have_http_status(500)
        error_msg = 'Reversal Transaction could not be created. ID provided does not exist.'
        expect(JSON.parse(response.body)['error']).to eq(error_msg)
      end
    end
  end
end
