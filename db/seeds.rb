# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Merchant.destroy_all

10.times do
	merchant = Merchant.find_or_initialize_by(email: Faker::Internet.email)
	if merchant.new_record?
		merchant.name = Faker::Name.unique.name
		merchant.password = 'qwerty23'
		merchant.save!
	end

	(2..10).to_a.sample.times do
		AuthorizeTransaction.create(uuid: SecureRandom.uuid, amount: (200..10000).to_a.sample, customer_email: Faker::Internet.email, customer_phone: Faker::PhoneNumber.cell_phone_in_e164, status: 'approved', merchant: merchant)
		ChargeTransaction.create(uuid: SecureRandom.uuid, amount: (200..10000).to_a.sample, customer_email: Faker::Internet.email, customer_phone: Faker::PhoneNumber.cell_phone_in_e164, status: 'approved', merchant: merchant)
		RefundedTransaction.create(uuid: SecureRandom.uuid, amount: (200..10000).to_a.sample, customer_email: Faker::Internet.email, customer_phone: Faker::PhoneNumber.cell_phone_in_e164, status: 'refunded', merchant: merchant)
		ReversalTransaction.create(uuid: SecureRandom.uuid, customer_email: Faker::Internet.email, customer_phone: Faker::PhoneNumber.cell_phone_in_e164, status: 'reversal', merchant: merchant)
	end
end