# == Schema Information
#
# Table name: payment_transactions
#
#  id                     :integer          not null, primary key
#  amount                 :float
#  customer_email         :string
#  customer_phone         :string
#  status                 :string
#  type                   :string
#  uuid                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  merchant_id            :integer
#  payment_transaction_id :integer
#
# Indexes
#
#  index_payment_transactions_on_merchant_id             (merchant_id)
#  index_payment_transactions_on_payment_transaction_id  (payment_transaction_id)
#  index_payment_transactions_on_status                  (status)
#  index_payment_transactions_on_type                    (type)
#  index_payment_transactions_on_uuid                    (uuid) UNIQUE
#
class PaymentTransaction < ApplicationRecord
  belongs_to :merchant, class_name: 'User'

  validates :uuid, presence: true, uniqueness: true
  validates :customer_email, format: { with: URI::MailTo::EMAIL_REGEXP }

  # Added :initiated here to cover the initial transaction before it is charged
  validates :status, inclusion: { in: %w(initiated approved reversed refunded error) }

  scoped_search on: %i(uuid status customer_phone customer_email)

  private

  def decrement_merchant_total
    merchant.update(total_transaction_sum: merchant.total_transaction_sum.to_f - amount)
  end

  def increment_merchant_total
    merchant.update(total_transaction_sum: merchant.total_transaction_sum.to_f + amount)
  end
end
