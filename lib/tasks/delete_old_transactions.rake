require_relative '../../app/jobs/delete_transactions_job'

namespace :transactions do
  desc 'Delete transactions older than an hour'
  task delete: :environment do
    DeleteTransactionsJob.perform_async
  end
end
