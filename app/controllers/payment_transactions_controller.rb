class PaymentTransactionsController < ApplicationController
  def index
    @title = 'Transactions'
    @title += " (#{params[:type]})" if params[:type].present?

    @payment_transactions = TransactionsService.call(params)
  end
end
