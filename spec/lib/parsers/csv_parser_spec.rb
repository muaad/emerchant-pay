require 'rails_helper'

require_relative '../../../lib/parsers/csv_parser'

describe Parsers::CsvParser do
  context 'CSV' do
    it 'parses file and returns an array of objects' do
      file = File.expand_path('../data/admins.csv', __dir__)
      records = described_class.parse_file(file: file, model: Admin)
      expect(records[0].name).to eq('Muaad')
      expect(records[0].description).to eq('some random guy')
    end
  end
end
