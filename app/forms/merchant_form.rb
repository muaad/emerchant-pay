class MerchantForm
  include ActiveModel::Model

  attr_accessor :name, :email, :description

  attr_reader :merchant

  validates :name, :email, presence: true
  validates :description, length: { maximum: 255 }

  def initialize(merchant, params = {})
    @merchant = merchant
    super(params)
  end

  def update
    @merchant.update(merchant_params)
  end

  private

  def merchant_params
    {
      name: name,
      email: email,
      description: description
    }
  end
end
