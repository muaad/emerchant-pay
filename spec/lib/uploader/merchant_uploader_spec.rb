require 'rails_helper'

require_relative '../../../lib/uploader/merchant_uploader'

describe Uploader::MerchantUploader do
  include_context 'users'

  context 'upload merchants' do
    it 'saves merchant records' do
      records = [merchant]
      expect { subject.upload(records: records) }.to change { Merchant.count }.by(1)
    end
  end
end
