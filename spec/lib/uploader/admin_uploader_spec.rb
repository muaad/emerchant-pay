require 'rails_helper'

require_relative '../../../lib/uploader/admin_uploader'

describe Uploader::AdminUploader do
  include_context 'users'

  context 'upload admins' do
    it 'saves admin records' do
      records = [admin]
      expect { subject.upload(records: records) }.to change { Admin.count }.by(1)
    end
  end
end
