require 'rails_helper'

require 'support/shared_contexts/users'

describe 'merchants#show', type: :system do
  include_context 'users'

  it 'shows the Merchant name and email on the page' do
    visit merchant_path(saved_merchant)

    expect(page).to have_content(saved_merchant.name)
    expect(page).to have_content(saved_merchant.email)
  end
end
