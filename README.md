# Introduction

This project is an effort to create a simple payment processing service.

### Versions

The project was built using Ruby 3.0.3 and Rails 7.0.2

## Setup

There is a `Makefile` you can consult for some quick commands to run but I will get into some of them in a bit.

### DB Setup

```ruby
rails db:create db:migrate
```

There is some seed data you can run 

```ruby
rails db:seed
```

but I only added that so I could see enough data appearing in the UI. It is better to just upload merchants and admins.

### Upload merchants and admins

There is a `rake` task (`users:upload`) you can run to upload merchants and admins. It takes two arguments: `file_name` and user `type` (`merchant` or `admin`). The CSV files are located in `lib/data`. Edit them to your heart's content.

```ruby
rake users:upload[merchants.csv,merchant]
rake users:upload[admins.csv,admin]
```
There is a quick shortcut for each that you can run with `make`

```
make upload.merchants
make upload.admins
```

## Run tests

```ruby
bundle exec rspec
```

Or

```
make test
```

## Run linter

```ruby
bundle exec rubocop
```

Or

```
make lint
```

## API Documentation

This is the kind of scenario I imagined when building the API:

Amazon.com is our customer and they use our services to process payments. When someone buys something on Amazon.com, Amazon sends us this API call:

```
POST /api/v1/payment_transactions
```

```json
{
    "api_key": "e9882e14740d4858ae6ead8be27845d1",
    "uuid": "34821391-0c9f-4155-9677-633b1a2af5e6",
    "amount": 300,
    "customer_email": "rian@example.com",
    "customer_phone": "234352323"
}
```

This creates an `AuthorizeTransaction` record in the database and returns the record itself in `JSON` format.

Amazon can then use the record ID to send us another request to charge the customer:

```
POST /api/v1/payment_transactions
```

```json
{
    "api_key": "e9882e14740d4858ae6ead8be27845d1",
    "payment_transaction_id": 20,
    "type": "Charge"
}
```
This will create a `ChargeTransaction` record, increment the merchant's `total_transaction_sum` by the transaction, set the original transaction's `status` to `approved` and returns the created record which, again, includes the ID.

If, for some reason, they want to refund the customer, they can send the following API call and include the ID of the `ChargeTransaction`:

```
POST /api/v1/payment_transactions
```

```json
{
    "api_key": "e9882e14740d4858ae6ead8be27845d1",
    "payment_transaction_id": 21,
    "type": "Refund"
}
```

This creates a `RefundTransaction` record, decrement the merchant's `total_transaction_sum` by the transaction amount, set the original transaction's `status` to `refunded` and returns the created record which, again, includes the ID.

To reverse a transaction, they will send the following call passing along the ID of the original `AuthorizeTransaction`:

```
POST /api/v1/payment_transactions
```

```json
{
    "api_key": "e9882e14740d4858ae6ead8be27845d1",
    "payment_transaction_id": 20,
    "type": "Reversal"
}
```
This creates a `ReversalTransaction` record, set the original transaction's `status` to `reversed` and returns the created record.

If they want to get a list of their transactions, they can send this:

```
GET /api/v1/payment_transactions?api_key=e9882e14740d4858ae6ead8be27845d
```

These API calls are only allowed through if the merchant's API key exists and if the merchant's `status` is set to `active`. This goes through one endpoint.

## Deleting old transactions

There is a `rake` task (`transactions:delete`) that calls a background worker (`Sidekiq` job) to delete any transactions older than an hour. This can be made to run hourly using the `whenever` [gem](https://github.com/javan/whenever). To put this task on the `crontab`, run this:

```ruby
bundle exec whenever RAILS_ENV=development
```
Make sure `Sidekiq` is running

```ruby
bundle exec sidekiq
```

And, the `Redis` server:

```
redis-server
```
