require 'csv'

module Parsers
  module CsvParser
    def self.parse_file(file:, model:)
      records = []
      CSV.foreach(file, headers: true, return_headers: false) do |row|
        records << model.new({
                               name: row[0].strip,
                               description: row[1].strip,
                               email: row[2].strip,
                               password: '1234567'
                             })
      end
      records
    end
  end
end
