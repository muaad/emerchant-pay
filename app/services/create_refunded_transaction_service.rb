class CreateRefundedTransactionService
  def self.call(id)
    transaction = ChargeTransaction.find_by(id: id)

    if transaction.nil?
      return { body: { error: 'Refund Transaction could not be created. ID provided does not exist.' },
               status: :internal_server_error }
    end

    refunded_transaction = RefundedTransaction.find_or_initialize_by(payment_transaction_id: id,
                                                                     merchant_id: transaction.merchant_id)

    unless refunded_transaction.new_record?
      return { body: { error: 'Refund Transaction already exists' },
               status: :internal_server_error }
    end

    refunded_transaction.uuid = SecureRandom.uuid
    refunded_transaction.amount = transaction.amount
    refunded_transaction.status = 'refunded'
    refunded_transaction.customer_email = transaction.customer_email
    refunded_transaction.customer_phone = transaction.customer_phone

    begin
      refunded_transaction.save!

      transaction.update(status: 'refunded')

      # Update the original transaction as well
      transaction.payment_transaction&.update(status: 'refunded')
    rescue StandardError
      return { body: { error: 'Refund Transaction could not be created. ID provided does not exist.' },
               status: :internal_server_error }
    end

    { body: refunded_transaction, status: 200 }
  end
end
