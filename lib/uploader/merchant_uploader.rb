module Uploader
  class MerchantUploader
    def upload(records:)
      # Active record import cannot run Active record callbacks other than
      # before_validation and after validation because it is mass importing
      # rows of data and doesn't necessarily have access to in-memory ActiveRecord objects.
      # So we need to tell it to run before_save on each item
      # https://github.com/zdennis/activerecord-import#callbacks

      records.each do |record|
        record.run_callbacks(:save) { false }
      end

      # We could have used Merchant.upsert_all (Rails 6+) instead of Merchant.import
      # but #upsert_all doesn't run validations

      Merchant.import(records, validate: true)
    end
  end
end
