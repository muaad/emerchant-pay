# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  api_key                :string
#  description            :text
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  status                 :string           default("inactive")
#  total_transaction_sum  :float
#  type                   :string           default("Merchant")
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_api_key               (api_key) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_status                (status)
#  index_users_on_type                  (type)
#

require 'rails_helper'

RSpec.describe User, type: :model do
  include_context 'users'

  context 'validations' do
    it 'validates presence of name' do
      expect(user).to be_valid
    end
  end

  context 'user types' do
    it 'returns true if admin' do
      expect(admin).to be_admin
    end

    it 'returns true if merchant' do
      expect(merchant).to be_merchant
    end

    it 'returns all merchants or admins' do
      expect(described_class.merchants).to match_array([saved_merchant, another_saved_merchant])
      expect(described_class.admins).to match_array([saved_admin])
    end
  end

  context 'API key' do
    it 'sets for merchants' do
      expect(saved_merchant.api_key).not_to be_nil
    end

    it 'does not set for admins' do
      expect(saved_admin.api_key).to be_nil
    end
  end
end
