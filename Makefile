test:
	bundle exec rspec

test.models:
	bundle exec rspec spec/models

test.requests:
	bundle exec rspec spec/requests

test.integration:
	bundle exec rspec spec/integration

test.lib:
	bundle exec rspec spec/lib

test.services:
	bundle exec rspec spec/services

lint:
	bundle exec rubocop

lint.fix:
	bundle exec rubocop --auto-correct

lint.fix_unsafe:
	bundle exec rubocop --auto-correct-all

mr: lint test

upload.merchants:
	rake users:upload[merchants.csv,merchant]

upload.admins:
	rake users:upload[admins.csv,admin]
