class DeleteTransactionsJob
  include Sidekiq::Job

  def perform
    Rails.logger.info "Running at: #{Time.zone.now}"
    Rails.logger.info 'Clearing older transactions'

    PaymentTransaction.where('created_at <= ?', 1.hour.ago).destroy_all

    Rails.logger.info 'Done clearing older transactions'
  end
end
