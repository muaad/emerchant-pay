# == Schema Information
#
# Table name: payment_transactions
#
#  id                     :integer          not null, primary key
#  amount                 :float
#  customer_email         :string
#  customer_phone         :string
#  status                 :string
#  type                   :string
#  uuid                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  merchant_id            :integer
#  payment_transaction_id :integer
#
# Indexes
#
#  index_payment_transactions_on_merchant_id             (merchant_id)
#  index_payment_transactions_on_payment_transaction_id  (payment_transaction_id)
#  index_payment_transactions_on_status                  (status)
#  index_payment_transactions_on_type                    (type)
#  index_payment_transactions_on_uuid                    (uuid) UNIQUE
#
FactoryBot.define do
  factory :refunded_transaction do
    uuid { SecureRandom.uuid }
    amount { 150 }
    status { 'refunded' }
    customer_email { 'customer@example.com' }
    customer_phone { '2222222' }
    payment_transaction { create(:charge_transaction) }

    after(:build) do |transaction|
      merchant = build(:merchant, password: '234242')
      merchant.save
      transaction.merchant = merchant
    end
  end
end
