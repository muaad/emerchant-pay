module Api
  module V1
    class ApiController < ApplicationController
      before_action :authenticate

      private

      def authenticate
        @merchant = Merchant.find_by(api_key: params[:api_key])
        unless @merchant
          render json: { error: 'Could not authenticate you' }, status: :unauthorized
        end

        if @merchant&.is_inactive?
          render json: { error: 'You need to be activated before you can access the API' }, status: :unauthorized
        end
      end
    end
  end
end
