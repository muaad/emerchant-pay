class HandleTransactionsService
  def self.call(params, merchant_id)
    return CreateTransactionService.call(params, merchant_id) if params[:uuid].present?

    if params[:payment_transaction_id].present?
      case params[:type]
      when 'Charge'
        CreateChargeTransactionService.call(params[:payment_transaction_id])
      when 'Refund'
        CreateRefundedTransactionService.call(params[:payment_transaction_id])
      when 'Reversal'
        CreateReversalTransactionService.call(params[:payment_transaction_id])
      else
        { body: { error: 'Unknown transaction type' }, status: :internal_server_error }
      end
    else
      { body: { error: 'Missing payment_transaction_id or a uuid' }, status: :internal_server_error }
    end
  end
end
